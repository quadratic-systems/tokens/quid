// SPDX-License-Identifier: GPL-3.0-or-later
pragma solidity ^0.8.25;

import {Test, console} from "forge-std/Test.sol";
import {QuId} from "../src/QuId.sol";
import {QuIdScript} from "../script/QuId.s.sol";
import {IAccessControl} from "@openzeppelin/contracts/access/IAccessControl.sol";

contract QuadraticTest is Test {
    QuId token;

    address deployer = address(this);

    uint256 bobPrivateKey = 0xB0B;
    uint256 alicePrivateKey = 0xA11CE;

    address bob = vm.addr(bobPrivateKey);
    address alice = vm.addr(alicePrivateKey);

    function setUp() public {
        QuIdScript script = new QuIdScript();
        token = script.run();
    }

    function testZeroBalanceOnDeployment() public view {
        assertEq(token.totalSupply(), 0);
    }

    function testMint() public {
        token.safeMint(bob, "http://bobs-archive.tld/");
        assertEq(token.tokenOfOwnerByIndex(bob, 0), 0);
    }

    function testURIResolution() public {
        token.safeMint(bob, "http://bobs-archive.tld/");
        assertEq(token.tokenURI(0), "http://bobs-archive.tld/");
    }

    function testTransfer() public {
        token.safeMint(bob, "http://bobs-archive.tld/");
        vm.prank(bob);
        token.safeTransferFrom(bob, alice, 0);
        assertEq(token.tokenOfOwnerByIndex(alice, 0), 0);
    }

    function testChangeURI() public {
        token.safeMint(bob, "http://bobs-archive.tld/");
        uint256 tokenId = token.tokenOfOwnerByIndex(bob, 0);
        token.setTokenURI(tokenId, "http://changed.bobs-archive.tld/");
        assertEq(token.tokenURI(tokenId), "http://changed.bobs-archive.tld/");
    }

    function testChangeURIWithInvalidAccount() public {
        token.safeMint(bob, "http://bobs-archive.tld/");
        uint256 tokenId = token.tokenOfOwnerByIndex(bob, 0);

        vm.expectRevert(
            abi.encodeWithSelector(IAccessControl.AccessControlUnauthorizedAccount.selector, bob, token.MINTER_ROLE())
        );
        vm.prank(bob);
        token.setTokenURI(tokenId, "http://changed.bobs-archive.tld/");
    }
}
