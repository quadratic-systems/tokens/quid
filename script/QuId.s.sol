// SPDX-License-Identifier: GPL-3.0-or-later
pragma solidity ^0.8.25;

import {Script, console} from "forge-std/Script.sol";
import {QuId} from "../src/QuId.sol";

contract QuIdScript is Script {
    function run() external returns (QuId) {
        vm.startBroadcast();
        QuId quid = new QuId(msg.sender);
        vm.stopBroadcast();

        return quid;
    }
}
